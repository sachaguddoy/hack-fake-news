var gulp = require("gulp"),
    watch = require("gulp-watch"),
    livereload = require('gulp-livereload');

gulp.task("dev", function() {
    livereload.listen();
    gulp.watch("*.js", []);
});
