const ItemInfoView = Backbone.View.extend({
    className: "info",
    tagName: "section",
    template: _.template(`
        <section class="info__controls">
            <button type="button" class="button button__primary js-play-in">Play</button>
            <button type="button" class="button button__secondary js-play-out">Play Out</button>
        </section>
        <% if(typeof image !== "undefined") { %>
            <div class="info__image">
                <img width="250" src="<%= image %>"/>
            </div>
        <% } %>
        <div class="info__details">
            <h3 class="info__name"><%= name %></h3>
            <% if(typeof description !== "undefined") { %>
                <h3 class="info__item"><%= description %></h3>
            <% } %>
            <% if(typeof url !== "undefined") { %>
                <h3 class="info__item">
                    <a href="<%= url %>"><%= url %></a>
                </h3>
            <% } %>
        </div>
        <% if(typeof articles !== "undefined") { %>
            <h4>Articles</h4>
            <ul class="info__articles">
                <% _.each(articles, function(article) { %>
                    <li class="info__article">
                        <div class="info__articleHeader"><%= article.title %></div>
                        <a href="<%= article.link %>" class="info__articleLink"><%= article.link %></a>
                    </li>
                <% }); %>
            </ul>
        <% } %>
    `),

    events: {
        "click .js-play-in": "triggerPlay",
        "click .js-play-out": "triggerPlayOut",
    },

    render: function() {
        this.$el.html(this.template(this.model.toJSON()));

        return this;
    },

    triggerPlay: function() {
        this.model.triggerPlay();
    },

    triggerPlayOut: function() {
        this.model.triggerPlayOut();
    }
});

const ModeratorView = Backbone.View.extend({
    className: "moderator",
    tagName: "section",
    template: _.template(`
        <div class="js-list list-container"></div>
        <div class="js-info info-container"></div>
    `),

    initialize: function(options) {
        this.listView = new ListView({
            collection: this.collection
        });

        this.collection.on("change:active", this.createInfoView, this);
    },

    render: function() {
        this.$el.html(this.template())
        this.$(".js-list").html(this.listView.render().$el);
        this.createInfoView();

        return this;
    },

    createInfoView: function(model) {
        if(_.isUndefined(model)) {
            model = this.collection.findWhere({ active: true });
            if(_.isUndefined(model)) return;
        }
        this.infoView = new ItemInfoView({ model });
        this.renderInfoView();
    },

    renderInfoView: function() {
        if(_.isUndefined(this.infoView)) return;

        this.$(".js-info").html(this.infoView.render().$el);
    }
});

const ListView = Backbone.View.extend({
    className: "list",
    tagName: "ul",

    initialize: function(options) {
        this.collection.on("add remove", this.render, this);
    },

    render: function() {
        this.$el.empty();
        this.collection.each(this.renderItem, this);
        return this;
    },

    renderItem: function(model) {
        const modelView = new ListItemView({ model });
        this.$el.append(modelView.render().$el);
    }
});

const ListItemView = Backbone.View.extend({
    template: _.template(`
        <li class="list__item" data-id="<%= id %>">
            <h2 class="list__itemName"><%= name %></h2>
        </li>
    `),

    events: {
        "click" : "onClick"
    },

    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },

    onClick: function() {
        this.model.set("active", true);
    }
});

const ItemModel = Backbone.Model.extend({
    initialize: function(options) {
        this.database = window.database;
        this.databaseRef = this.database.ref("/people/" + this.id);
        this.databaseRef.on("value", this.onSync, this);
    },

    onSync: function(snapshot) {
        this.set(snapshot.val());
    },

    triggerPlay: function() {
        this.set("activeInPlayer", true);
        this.databaseRef.update({ "activeInPlayer": true });
    },

    triggerPlayOut: function(silent = false) {
        this.set("activeInPlayer", false, { silent });
        this.databaseRef.update({ "activeInPlayer": false });
    }
});

const ItemsCollection = Backbone.Collection.extend({
    comparator: model => {
        return -parseFloat(model.get("timestamp"));
    },

    model: ItemModel,
    initialize: function() {
        this.on("change:active", this.onChangeActiveItem);
        this.on("change:activeInPlayer", this.onChangeItemActiveInPlayer);
        this.database = window.database;
        const peopleRef = this.database.ref("/people/");

        peopleRef.on("child_added", this.onPersonAdded, this);
        peopleRef.on("child_changed", this.onPersonChanged, this);
        peopleRef.on("child_removed", this.onPersonRemoved, this);
    },

    onPersonAdded: function(snapshot) {
        const newPerson = snapshot.val();
        newPerson.id = snapshot.key;
        this.add(newPerson, { parse: true });
    },

    onPersonChanged: function(snapshot) {
        const model = this.get(snapshot.key);
        const newValues = snapshot.val();
        model.set(snapshot.val(), { parse: true });
        this.sort();
    },

    onPersonRemoved: function(snapshot) {
        this.remove(snapshot.key);
    },

    onChangeActiveItem: function(activeModel) {
        this.each(model => {
            if(model.id === activeModel.id) {
                return;
            }
            if(model.get("active")) {
                model.set("active", false);
            }
        });
    },

    onChangeItemActiveInPlayer: function(item) {
        this.each(model => {
            if(item.id === model.id) {
                return;
            }
            if(model.get("activeInPlayer")) {
                model.triggerPlayOut(true);
            }
        });
    }
});


$body = $("body");

const items = new ItemsCollection([]);
const moderatorView = new ModeratorView({ collection: items });
$body.append(moderatorView.render().$el);
